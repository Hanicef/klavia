/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bytes"
	"testing"
)

type TestGenerator struct {
	Categories map[string][]byte
	Documents  DocumentList
	Resources  ResourceMap
}

func (t *TestGenerator) FrontPage() *Response {
	return &Response{
		Status:      Ok,
		Content:     []byte("front page"),
		ContentType: "text/plain",
	}
}

func (t *TestGenerator) Category(cat string) *Response {
	data, ok := t.Categories[cat]
	if !ok {
		return t.NotFound()
	}
	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: "text/plain",
	}
}

func (t *TestGenerator) Document(file string, raw bool) *Response {
	doc, ok := t.Documents.Find(file)
	if !ok {
		return t.NotFound()
	}
	ctype := "text/gemini"
	if raw {
		ctype = "text/plain"
	}
	return &Response{
		Status:      Ok,
		Content:     []byte(doc.Path),
		ContentType: ctype,
	}
}

func (t *TestGenerator) Resource(path string) *Response {
	r, ok := t.Resources[path]
	if !ok {
		return t.NotFound()
	}
	return &Response{
		Status:      Ok,
		Content:     []byte(r),
		ContentType: "text/plain",
	}
}

func (t *TestGenerator) NotFound() *Response {
	return &Response{
		Status:      NotFound,
		Content:     []byte("not found"),
		ContentType: "text/plain",
	}
}

func TestHandleClient(t *testing.T) {
	testResponse := func(actual *Response, expect *Response) {
		if actual.Status != expect.Status {
			t.Errorf("actual.Status != %d (%d)", expect.Status, actual.Status)
		}
		if bytes.Compare(actual.Content, expect.Content) != 0 {
			t.Errorf("actual.Content != %s (%s)", string(expect.Content), string(actual.Content))
		}
		if actual.ContentType != expect.ContentType {
			t.Errorf("actual.ContentType != %s (%s)", expect.ContentType, actual.ContentType)
		}
		if expect.Status == Redirect && actual.Redirect != expect.Redirect {
			t.Errorf("actual.Redirect != %s (%s)", expect.Redirect, actual.Redirect)
		}
	}
	g := TestGenerator{
		Categories: map[string][]byte{
			"foo": []byte("foo"),
			"bar": []byte("bar"),
		},
		Documents: DocumentList{
			Document{
				Name:  "bar",
				Title: "Bar",
				Path:  "/bar",
			},
			Document{
				Name:  "baz",
				Title: "Baz",
				Path:  "/baz",
			},
			Document{
				Name:  "foo",
				Title: "Foo",
				Path:  "/foo",
			},
		},
		Resources: ResourceMap{
			"/lorem": "lorem",
			"/ipsum": "ipsum",
		},
	}
	resp := HandleClient("/", &g)
	testResponse(resp, &Response{
		Status:      Ok,
		Content:     []byte("front page"),
		ContentType: "text/plain",
	})

	resp = HandleClient("/not-found", &g)
	testResponse(resp, &Response{
		Status:      NotFound,
		Content:     []byte("not found"),
		ContentType: "text/plain",
	})

	resp = HandleClient("/category/foo", &g)
	testResponse(resp, &Response{
		Status:      Ok,
		Content:     []byte("foo"),
		ContentType: "text/plain",
	})

	resp = HandleClient("/category/bar", &g)
	testResponse(resp, &Response{
		Status:      Ok,
		Content:     []byte("bar"),
		ContentType: "text/plain",
	})

	resp = HandleClient("/category/err", &g)
	testResponse(resp, &Response{
		Status:      NotFound,
		Content:     []byte("not found"),
		ContentType: "text/plain",
	})

	resp = HandleClient("/document/foo", &g)
	testResponse(resp, &Response{
		Status:      Ok,
		Content:     []byte("/foo"),
		ContentType: "text/gemini",
	})

	resp = HandleClient("/document/bar", &g)
	testResponse(resp, &Response{
		Status:      Ok,
		Content:     []byte("/bar"),
		ContentType: "text/gemini",
	})

	resp = HandleClient("/document/foo.txt", &g)
	testResponse(resp, &Response{
		Status:      Redirect,
		Content:     []byte("Redirecting to /document/raw/foo"),
		ContentType: "text/plain",
		Redirect:    "/document/raw/foo",
	})

	resp = HandleClient("/document/bar.txt", &g)
	testResponse(resp, &Response{
		Status:      Redirect,
		Content:     []byte("Redirecting to /document/raw/bar"),
		ContentType: "text/plain",
		Redirect:    "/document/raw/bar",
	})

	resp = HandleClient("/document/err", &g)
	testResponse(resp, &Response{
		Status:      NotFound,
		Content:     []byte("not found"),
		ContentType: "text/plain",
	})

	resp = HandleClient("/lorem", &g)
	testResponse(resp, &Response{
		Status:      Ok,
		Content:     []byte("lorem"),
		ContentType: "text/plain",
	})

	resp = HandleClient("/ipsum", &g)
	testResponse(resp, &Response{
		Status:      Ok,
		Content:     []byte("ipsum"),
		ContentType: "text/plain",
	})
}
