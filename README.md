# Klavia

Klavia is a multi-protocol server designed to serve documents over the internet.
It currently supports HTTP, Gopher and Gemini, and is very lightweight and
simply designed.

# Install

Golang must be installed to build the project.

```
go get gitlab.com/Hanicef/klavia
```

The executable should appear under $HOME/go/bin/klavia. To install, move the
file to either /usr/local/bin or, to install under the local user, $HOME/bin.
Remember to set appropriate privileges; executables under /usr should be owned
by root!

# Overview

Each document has an entry in what is called the *document table*. This table
contains information on what documents and categories that exist on the server,
and is used to find the physical location of the documents on the filesystem.

The server serves files from the following paths: /, /document/\* and
/category/\*.

The root path (/) loads the file from the `Front` tag, and appends links to all
categories at the end. For HTTP, this is then inserted in the `Layout` file.
The category paths (/category/\*) list documents within a category, where the
star (\*) represents the category key. The document paths (/document/\*) serve
the document and it's content, where the star (\*) represents the document key.

# Configure

The configuration file is an XML file. Example:

```
<?xml version="1.0" encoding="utf-8"?>
<Configuration>
	<TLS>
		<Cert>tls.cert</Cert>
		<Key>tls.key</Key>
	</TLS>
	<Document>
		<Table>/etc/klavia.d/doctab</Table>
		<Encoding>utf-8</Encoding>
		<Header>/etc/klavia.d/header.gmi</Header>
	</Document>
	<Gopher>
		<Hostname>example.com</Hostname>
		<Listen>:7070</Listen>
		<Pages>
			<Front>/etc/klavia.d/index.gop</Front>
			<NotFound>/etc/klavia.d/not-found.gop</NotFound>
		</Pages>
		<Resources>
			<Resource Path="/about">/etc/klavia.d/about.gop</Resource>
		</Resources>
		<Directories>
			<Directory Path="/assets">/etc/klavia.d/assets</Directory>
		</Directories>
	</Gopher>
	<Gemini>
		<Hostname>example.com</Hostname>
		<Listen>:1965</Listen>
		<FrontPage>/etc/klavia.d/index.gmi</FrontPage>
		<Resources>
			<Resource Path="/about">/etc/klavia.d/about.gmi</Resource>
		</Resources>
		<Directories>
			<Directory Path="/assets">/etc/klavia.d/assets</Directory>
		</Directories>
	</Gemini>
	<HTTP>
		<Hostname>example.com</Hostname>
		<Listen>:8080</Listen>
		<ListenTLS>:8443</ListenTLS>
		<Layout>/etc/klavia.d/layout.html</Layout>
		<Pages>
			<Front>/etc/klavia.d/index.html</Front>
			<NotFound>/etc/klavia.d/not-found.html</NotFound>
		</Pages>
		<Resources>
			<Resource Path="/style">/etc/klavia.d/style.css</Resource>
			<Resource Path="/about">/etc/klavia.d/about.html</Resource>
		</Resources>
		<Directories>
			<Directory Path="/assets">/etc/klavia.d/assets</Directory>
		</Directories>
	</HTTP>
	<Placeholders>
		<Placeholder Key="EXAMPLE">Example text</Placeholder>
	</Placeholders>
</Configuration>
```

## Fields

### TLS

Note that TLS must be configured for Gemini to run; if these fields are not
supplied, the Gemini server will not start.

#### Cert

Path to the TLS certificate (Public key).

#### Key

Path to the TLS private key that corresponds to the certificate.

### Document

#### Table

Path to the document table; more details can be found after this section.

#### Encoding

Specifies what encoding the document uses. Defaults to UTF-8 if not specified.

#### Header

Path to the header file. If present, the content of this file will be inserted
between the title and the content of a document. This is useful if you want to
place a copyright notice in every document.

### Gopher/HTTP/Gemini

#### Hostname

The FQDN that the server is exposed on; this is used internally to generate
responses that point to the correct host.

For HTTP and Gemini, clients connecting to the server is expected to match this
hostname; if this is not the case, the client will be redirected to the
specified hostname through a `308 Permanent Redirect` for HTTP or `31 Permanent
Redirect` for Gemini.

Excluding this field or leaving it blank will disable the respective server.

#### Listen

Host and port to listen to, in the format `host:port`. If the host is empty, the
server will listen on all addresses.

#### ListenTLS

On HTTP, specifies the host and port to listen to for TLS connections. Has no
effect if no TLS certificate is specified.

#### Pages

##### Front

Path to the front page file. This will be displayed to the client when the
front page is visited.

##### NotFound

Path to the missing page file. This will be displayed to the client when an
invalid file path is specified. Note that this is ignored in Gemini, due to the
protocol not sending a document when a 51 (not found) status code is sent to the
client.

#### Resources

Contains one or more `Resource` tag. Resources are used to map additional files
to URL paths. The `Path` attribute specifies the URL path that this resource is
located at, and the text inside the tag is the filesystem path to the file.

#### Directories

Contains one or more `Directory` tag. Directories are files that are served
relative to the path specified in the `Path` attribute. The requested path will
then be translated to the physical path specified within this tag, and if a file
is located at that path, it's served to the client.

Be warned, though, that while files outside of this directory cannot be served
directly, it can still be accessed indirectly through other means like symbolic
links. Because of this, make sure that the directory is isolated and that you
have full control of the directory that is being served!

### Placeholders

Contains `Placeholder` tags, which allows adding custom placeholders into the
templating engine. The `Key` attribute defines the placeholder key, which is the
string that is used after the `$`. Each placeholder will then be replaced by the
text inside the `Placeholder` tag as described in "Templating" section.

## Document table

The document table specifies the documents and categories to be displayed. The
formatting is line-by-line with a very simple syntax. Example:

```
# Comment
Category example
	Title Example
	Description Example category

Document foo
	Title Foo
	Path /etc/klavia.d/foo
	Category example
```

A section starts with a non-indented line containing the section type and name.
This is followed by lines indented with one or more tabs or spaces, specifying
a field name and value.

### Category

A category is listed on the front page, after the main content.

#### Title

The title of the category. This will be displayed to the client.

#### Description

The description of the category. This will be displayed to the client.

### Document

A document is listed in the category it resides in.

#### Title

The title of the document. This will be displayed to the client.

#### Path

The path to the file on the filesystem.

#### Category

The category the document belongs to. While this is not a mandatory field, not
specifying this will prevent the document from appearing in the document list.
However, it can still be accessed directly through a URL path.

Unlike other fields, the `Category` field may appear multiple times, and the
document will be assigned to all of the categories specified. This is useful
for documents that fall into multiple categories.

## Templating

The server uses a basic templating system that searches for tags and replaces
them with appropriate values. An example layout that uses tags:

```
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>$HOST - Example website</title>
	</head>
	<body>$CONTENT</body>
</html>
```

All `$` followed by letters are placeholders, and are replaced during document
generation. Note that placeholders are not used in documents, and are case
sensitive.

### $HOST

The $HOST placeholder is replaced by the hostname specified in the
configuration file. The hostname used depends on what protocol is used.

### $PORT

The $PORT placeholder is replaced by the port that the server is listening on.
Mostly used in Gopher due to the port being required for links.

### $HTTPHOST

Like $HOST, but always uses the HTTP host, regardless of protocol.

### $HTTPPORT

Like $PORT, but always uses the HTTP port, regardless of protocol.

### $HTTPSPORT

Like $PORT, but always uses the HTTPS port, regardless of protocol.

### $GOPHERHOST

Like $HOST, but always uses the Gopher host, regardless of protocol.

### $GOPHERPORT

Like $PORT, but always uses the Gopher port, regardless of protocol.

### $GEMINIHOST

Like $HOST, but always uses the Gemini host, regardless of protocol.

### $GEMINIPORT

Like $PORT, but always uses the Gemini port, regardless of protocol.

### $REQPATH

$REQPATH is replaced with the virtual path to the current document.

### $CONTENT

$CONTENT is only used in the HTTP layout file, and is replaced by the page
content. Using it at any other place will be ignored.

### $DOCCOUNT

$DOCCOUNT is replaced with the amount of documents that has been registered in
the document table.

### $CATCOUNT

$CATCOUNT is replaced with the amount of categories that has been registered in
the document table.

### $DATE

$DATE is replaced by the current date of the server, in the format YYYY-MM-DD.

### $TIME

$TIME is replaced by the current time of the server, in the format HH:MM:SS.

### $TIMEZONE

$TIMEZONE is replaced by the timezone the server is located in.

### $UPTIME

$UPTIME is replaced by the amount of days that the server has been running.

## Document format

The format used by documents is the Gemtext format, which is the official format
used by the Gemini protocol. The format is described at
gemini://gemini.circumlunar.space/docs/gemtext.gmi

# Run

To run the server, invoke:

```
klavia -config /path/to/config.xml
```

If you wish to log the output to a file, pass the `-log` command line argument
to the program.

The program responds to two signals: SIGUSR1 and SIGTERM. SIGUSR1 signals the
server to reload the configuration. If the server fails to reload the
configuration file, it will revert back to the old configuration and continue
running. SIGTERM signals graceful shutdown, which means that it waits for all
clients to finish their session before shutting down the server.
