/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var MetaResp []byte
var GopherPort []byte

var GopherListener net.Listener
var IsGopherStopping bool

var GopherWait sync.WaitGroup

type GopherGenerator int

func (g GopherGenerator) FrontPage() *Response {
	data, err := ioutil.ReadFile(Config.Gopher.Pages.Front)
	if err != nil {
		Log("Gopher: Front page is missing")
		return g.Error()
	}

	data = bytes.Replace(data, []byte("\n"), []byte("\r\n"), -1)
	for _, c := range Categories {
		data = append(data, '1')
		data = append(data, c.Title...)
		data = append(data, '\t')
		data = append(data, CategoryPrefix...)
		data = append(data, c.Name...)
		data = append(data, '\t')
		data = append(data, Config.Gopher.Hostname...)
		data = append(data, '\t')
		data = append(data, GopherPort...)
		data = append(data, '\r', '\n')
	}
	data = append(data, '.', '\r', '\n')
	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: "text/x-gopher",
	}
}

func (g GopherGenerator) Category(cat string) *Response {
	if c, ok := Categories.Find(cat); ok {
		var data = []byte("i  ")
		data = append(data, c.Title...)
		data = append(data, "\t/\tnil.host\t70\r\n"...)
		data = append(data, "i\t/\tnil.host\t70\r\n"...)
		if c.Description != "" {
			i := 0
			for i < len(c.Description) {
				for c.Description[i] == ' ' {
					i++
				}

				end := i + 80
				if end > len(c.Description) {
					end = len(c.Description)
				}

				if end < len(c.Description) {
					// Avoid splitting words into lines
					for c.Description[end] != ' ' {
						end--
					}
				}

				data = append(data, 'i')
				data = append(data, c.Description[i:end]...)
				data = append(data, "\t/\tnil.host\t70\r\n"...)
				i = end + 1
			}
			data = append(data, "i\t/\tnil.host\t70\r\n"...)
		}

		for _, d := range c.Documents {
			data = append(data, '0')
			data = append(data, d.Title...)
			data = append(data, '\t')
			data = append(data, DocumentPrefix...)
			data = append(data, d.Name...)
			data = append(data, '\t')
			data = append(data, Config.Gopher.Hostname...)
			data = append(data, '\t')
			data = append(data, GopherPort...)
			data = append(data, '\r', '\n')
		}
		data = append(data, "i\t/\tnil.host\t70\r\n"...)
		data = append(data, "1<-- Go back\t/\t"...)
		data = append(data, Config.Gopher.Hostname...)
		data = append(data, '\t')
		data = append(data, GopherPort...)
		data = append(data, '\r', '\n', '.', '\r', '\n')
		return &Response{
			Status:      Ok,
			Content:     data,
			ContentType: "text/x-gopher",
		}
	}

	return g.NotFound()
}

func (g GopherGenerator) Document(file string, _ bool) *Response {
	doc, ok := Documents.Find(file)
	if !ok {
		return g.NotFound()
	}

	data, err := doc.Load()
	if err != nil {
		Logf("Gopher: Document %s is missing", doc.Name)
		return g.Error()
	}

	data = bytes.Replace(data, []byte("\n"), []byte("\r\n"), -1)
	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: "text/plain",
	}
}

func (g GopherGenerator) Resource(file string) *Response {
	res, ok := Config.Gopher.Resources[file]
	if !ok {
		res, ok = Config.HTTP.Directories.Retrieve(file)
		if !ok {
			return g.NotFound()
		}

		if stat, err := os.Stat(res); err != nil || stat.IsDir() {
			if err == nil || errors.Is(err, os.ErrNotExist) {
				return g.NotFound()
			}
			Logf("Gopher: Resource %s is inaccessible: %s", res, err)
			return g.Error()
		}
	}

	data, err := ioutil.ReadFile(res)
	if err != nil {
		Logf("Gopher: %s: Read error: %s", res, err)
		return g.Error()
	}

	data = bytes.Replace(data, []byte("\n"), []byte("\r\n"), -1)
	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: "text/plain",
	}
}

func (g GopherGenerator) NotFound() *Response {
	data, err := ioutil.ReadFile(Config.Gopher.Pages.NotFound)
	if err != nil {
		Log("Gopher: Not found page is missing")
		return g.Error()
	}
	data = bytes.Replace(data, []byte("\n"), []byte("\r\n"), -1)
	data = append(data, '.', '\r', '\n')
	return &Response{
		Status:      NotFound,
		Content:     data,
		ContentType: "text/x-gopher",
	}
}

func (g GopherGenerator) Error() *Response {
	return &Response{
		Status:      Error,
		Content:     []byte("3An internal server error has occured!\t/\tnil.host\t70\r\n.\r\n"),
		ContentType: "text/x-gopher",
	}
}

func ReloadGopher(cfg *Configuration) {
	MetaResp = []byte("+-1\r\n+INFO: 1")
	MetaResp = append(MetaResp, cfg.Gopher.Hostname...)
	MetaResp = append(MetaResp, '\t', '/', '\t')
	MetaResp = append(MetaResp, cfg.Gopher.Hostname...)
	MetaResp = append(MetaResp, '\t')
	MetaResp = append(MetaResp, GopherPort...)
	MetaResp = append(MetaResp, "\r\n"...)

	if Config.Gopher.Listen != cfg.Gopher.Listen {
		Log("Gopher: A server restart is required to change the listening address")
	}
}

func StopGopher() {
	if GopherListener != nil {
		IsGopherStopping = true
		GopherListener.Close()
		GopherWait.Wait()
	}
}

func StartGopher() {
	listenAddr := Config.Gopher.Listen
	if Config.Gopher.Listen == "" {
		Config.Gopher.Listen = ":70"
	}
	var err error
	GopherListener, err = net.Listen("tcp", listenAddr)
	if err != nil {
		Log("Gopher: ", err)
		return
	}

	GopherPort = []byte(strconv.Itoa(GopherListener.Addr().(*net.TCPAddr).Port))
	ReloadGopher(Config)

	for {
		conn, err := GopherListener.Accept()
		if err != nil {
			if IsGopherStopping {
				break
			}
			Log("Gopher: ", err)
			continue
		}

		GopherWait.Add(1)
		go func(conn net.Conn) {
			defer conn.Close()
			ConfigLock.RLock()
			defer ConfigLock.RUnlock()
			defer GopherWait.Done()

			err := conn.SetReadDeadline(time.Now().Add(ReadTimeout))
			if err != nil {
				Log("Gopher: ", err)
				return
			}

			line, err := ReadLine(conn)
			if err != nil {
				if neterr, ok := err.(net.Error); !ok || !neterr.Timeout() {
					Log("Gopher: ", err)
				}
				return
			}

			err = conn.SetWriteDeadline(time.Now().Add(WriteTimeout))
			if err != nil {
				Log("Gopher: ", err)
				return
			}

			if strings.HasSuffix(line, "\t$") {
				_, err = conn.Write(MetaResp)
				if err != nil {
					Log("Gopher: ", err)
				}
				return
			}

			tab := strings.Index(line, "\t")
			if tab != -1 {
				line = line[:tab]
			}

			if len(line) == 0 {
				line = "/"
			} else if line[0] != '/' {
				_, err = conn.Write([]byte("3Request must start with /	nil	nil	nil\r\n.\r\n"))
				if err != nil {
					Log("Gopher: ", err)
				}
				return
			}

			Log("Gopher: ", conn.RemoteAddr().String(), " -> ", line)

			resp := HandleClient(line, GopherGenerator(0))
			if resp.Content != nil {
				t := NewTemplate(resp.Content)
				m := StandardPlaceholders()
				m[HostPlaceholder] = []byte(Config.Gopher.Hostname)
				m[PortPlaceholder] = GopherPort
				m[ReqPathPlaceholder] = []byte(line)
				resp.Content = t.Apply(m)
				_, err = conn.Write(resp.Content)
				if err != nil {
					Log("Gopher: ", err)
					return
				}
			}
		}(conn)
	}
}
