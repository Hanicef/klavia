/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"sync"
	"time"
)

const ReadTimeout = time.Second * 15
const WriteTimeout = time.Second * 300

const MaxLineLength = 255

var LogFile = flag.String("log", "", "path to write log files to")
var logMutex = sync.Mutex{}

func ReadLine(r io.Reader) (string, error) {
	var b [1]byte
	var s []byte
	for len(s) <= MaxLineLength {
		_, err := r.Read(b[:])
		if err != nil {
			return "", err
		}

		if b[0] == '\r' {
			_, err = r.Read(b[:])
			if err != nil {
				return "", err
			}

			if b[0] == '\n' {
				return string(s), nil
			}
			s = append(s, '\r')
		}
		s = append(s, b[0])
	}

	return "", errors.New("line too long")
}

func openLog() (*os.File, error) {
	if *LogFile == "" {
		return os.Stdout, nil
	} else {
		return os.OpenFile(*LogFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	}
}

func Log(a ...interface{}) int {
	logMutex.Lock()
	defer logMutex.Unlock()
	log, err := openLog()
	if err != nil {
		panic("Log: " + err.Error())
	}

	n, err := fmt.Fprint(log, "[", time.Now().Format("2006-01-02 15:04:05"), "] ")
	if err != nil {
		panic("Log: " + err.Error())
	}
	v, err := fmt.Fprint(log, a...)
	if err != nil {
		panic("Log: " + err.Error())
	}
	n += v
	v, err = fmt.Fprintln(log)
	if err != nil {
		panic("Log: " + err.Error())
	}
	if *LogFile != "" {
		log.Close()
	}
	return n + v
}

func Logf(format string, a ...interface{}) int {
	logMutex.Lock()
	defer logMutex.Unlock()
	log, err := openLog()
	if err != nil {
		panic("Log: " + err.Error())
	}

	n, err := fmt.Fprint(log, "[", time.Now().Format("2006-01-02 15:04:05"), "] ")
	if err != nil {
		panic("Logf: " + err.Error())
	}
	v, err := fmt.Fprintf(log, format, a...)
	if err != nil {
		panic("Logf: " + err.Error())
	}
	n += v
	v, err = fmt.Fprintln(log)
	if err != nil {
		panic("Logf: " + err.Error())
	}
	if *LogFile != "" {
		log.Close()
	}
	return n + v
}

func GetDocumentType(f string) (string, error) {
	file, err := os.Open("/etc/mime.types")
	if err != nil {
		return "", err
	}
	defer file.Close()
	stream := bufio.NewReader(file)
	e := path.Ext(f)
	if e != "" {
		e = e[1:]
		for {
			line, err := stream.ReadString('\n')
			if err != nil {
				if err == io.EOF {
					break
				}
				return "", err
			}
			if len(line) == 0 || line[0] == '#' {
				continue
			}

			fields := strings.Fields(line)
			if len(fields) < 2 {
				continue
			}
			for _, f = range fields[1:] {
				if f == e {
					return fields[0], nil
				}
			}
		}
	}

	return "application/octet-stream", nil
}
