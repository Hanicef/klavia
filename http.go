/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"github.com/andybalholm/brotli"

	"bytes"
	"compress/flate"
	"compress/gzip"
	"compress/lzw"
	"context"
	"crypto/md5"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	LayoutContent = "CONTENT"
)

type Range struct {
	Start int64
	End   int64
}

var (
	HTTPServer  http.Server
	HTTPSServer http.Server
)

var (
	HTTPPort  []byte
	HTTPSPort []byte
)

func ConvertGeminiToHTML(data []byte) []byte {
	var (
		out  []byte
		pre  bool
		ul   bool
		quot bool
	)

	for {
		line := data
		var i int
		for i = 0; i < len(line) && line[i] != '\n'; i++ {
		}
		if i == len(line) {
			break
		}
		line = line[:i]
		data = data[i+1:]
		line = bytes.Replace(line, []byte("&"), []byte("&amp;"), -1)
		line = bytes.Replace(line, []byte("<"), []byte("&lt;"), -1)

		if bytes.HasPrefix(line, []byte("```")) {
			if !pre {
				out = append(out, "<pre>"...)
			} else {
				out = append(out, "</pre>"...)
			}
			pre = !pre
			continue
		} else if pre {
			out = append(out, line...)
			out = append(out, '\r', '\n')
			continue
		}

		if bytes.HasPrefix(line, []byte("* ")) {
			if !ul {
				out = append(out, "<ul>"...)
				ul = true
			}
			out = append(out, "<li>"...)
			out = append(out, line[2:]...)
			out = append(out, "</li>"...)
			continue
		} else if ul {
			out = append(out, "</ul>"...)
			ul = false
		}

		if bytes.HasPrefix(line, []byte(">")) {
			if !quot {
				out = append(out, "<blockquote>"...)
				quot = true
			}
			out = append(out, line[2:]...)
			out = append(out, "<br/>"...)
			continue
		} else if quot {
			out = append(out, "</blockquote>"...)
			quot = false
		}

		if bytes.HasPrefix(line, []byte("=>")) {
			line = bytes.TrimSpace(line[2:])
			s := bytes.IndexByte(line, ' ')
			var c []byte
			if s != -1 {
				c = bytes.TrimSpace(line[s:])
				line = line[:s]
				if len(c) == 0 {
					c = line
				}
			} else {
				c = line
			}
			out = append(out, "<a href=\""...)
			out = append(out, line...)
			out = append(out, '"', '>')
			out = append(out, c...)
			out = append(out, "</a><br/>"...)
		} else if bytes.HasPrefix(line, []byte("#")) {
			for i = 1; i < len(line) && line[i] == '#'; i++ {
			}
			out = append(out, '<', 'h', byte('0'+i), '>')
			out = append(out, bytes.TrimSpace(line[i:])...)
			out = append(out, '<', '/', 'h', byte('0'+i), '>')
		} else {
			out = append(out, line...)
			out = append(out, "<br/>"...)
		}
	}
	if quot {
		out = append(out, "</blockquote>"...)
	}
	if ul {
		out = append(out, "</ul>"...)
	}
	return out
}

type HTTPGenerator int

func (g HTTPGenerator) FrontPage() *Response {
	data, err := ioutil.ReadFile(Config.HTTP.Pages.Front)
	if err != nil {
		Log("HTTP: Front page is missing")
		return g.Error()
	}

	for _, c := range Categories {
		data = append(data, "<div class=\"category\"><a href=\""...)
		data = append(data, CategoryPrefix...)
		data = append(data, c.Name...)
		data = append(data, '"', '>')
		data = append(data, c.Title...)
		data = append(data, "</a></div>"...)
	}
	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: "text/html; charset=" + Config.Document.Encoding,
	}
}

func (g HTTPGenerator) Category(cat string) *Response {
	if c, ok := Categories.Find(cat); ok {
		var data = []byte("<h1>")
		data = append(data, c.Title...)
		data = append(data, "</h1>"...)
		if c.Description != "" {
			data = append(data, "<p class=\"description\">"...)
			data = append(data, c.Description...)
			data = append(data, "</p>"...)
		}

		for _, d := range c.Documents {
			data = append(data, "<div class=\"document\"><a href=\""...)
			data = append(data, DocumentPrefix...)
			data = append(data, d.Name...)
			data = append(data, '"', '>')
			data = append(data, d.Title...)
			data = append(data, "</a> <a href=\""...)
			data = append(data, RawPrefix...)
			data = append(data, d.Name...)
			data = append(data, "\">[source]</a></div>"...)
		}
		data = append(data, "<p><a href=\"/\">&lt;-- Go back</a></p>"...)
		return &Response{
			Status:      Ok,
			Content:     data,
			ContentType: "text/html; charset=" + Config.Document.Encoding,
		}
	}

	return g.NotFound()
}

func (g HTTPGenerator) Document(file string, raw bool) *Response {
	doc, ok := Documents.Find(file)
	if !ok {
		return g.NotFound()
	}

	data, err := doc.Load()
	if err != nil {
		Logf("HTTP: Document %s is missing", doc.Name)
		return g.Error()
	}

	if raw {
		return &Response{
			Status:      Ok,
			Content:     data,
			ContentType: "text/plain; charset=" + Config.Document.Encoding,
		}
	}

	return &Response{
		Status:      Ok,
		Content:     ConvertGeminiToHTML(data),
		ContentType: "text/html; charset=" + Config.Document.Encoding,
	}
}

func (g HTTPGenerator) Resource(file string) *Response {
	res, ok := Config.HTTP.Resources[file]
	if !ok {
		res, ok = Config.HTTP.Directories.Retrieve(file)
		if !ok {
			return g.NotFound()
		}

		if stat, err := os.Stat(res); err != nil || stat.IsDir() {
			if err == nil || errors.Is(err, os.ErrNotExist) {
				return g.NotFound()
			}
			Logf("HTTP: Resource %s is inaccessible: %s", res, err)
			return g.Error()
		}
	}

	data, err := ioutil.ReadFile(res)
	if err != nil {
		Logf("HTTP: %s: Read error: %s", res, err)
		return g.Error()
	}

	ctype, err := GetDocumentType(res)
	if err != nil {
		Logf("HTTP: Cannot determine MIME type: %s", err)
		return g.Error()
	}

	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: ctype,
	}
}

func (g HTTPGenerator) NotFound() *Response {
	data, err := ioutil.ReadFile(Config.HTTP.Pages.NotFound)
	if err != nil {
		Log("HTTP: Not found page is missing")
		return g.Error()
	}
	return &Response{
		Status:      NotFound,
		Content:     data,
		ContentType: "text/html; charset=" + Config.Document.Encoding,
	}
}

func (g HTTPGenerator) Error() *Response {
	return &Response{
		Status:      Error,
		Content:     []byte("500 Internal server error"),
		ContentType: "text/plain; charset=" + Config.Document.Encoding,
	}
}

type httpLog int

func (_ httpLog) Write(p []byte) (n int, err error) {
	return Log(string(p[:len(p)-1])), nil
}

func parseAccept(accept string) []string {
	var (
		values = strings.Split(accept, ",")
		keys   []int
		parsed = make(map[int][]string, len(values))

		add = func(k int, v string) {
			if arr, ok := parsed[k]; ok {
				arr = append(arr, v)
				parsed[k] = arr
			} else {
				parsed[k] = []string{v}
				keys = append(keys, k)
			}
		}
	)
	for _, s := range values {
		if delim := strings.Index(s, ";"); delim != -1 {
			ctype := strings.TrimSpace(s[:delim])
			v := s[delim+1:]
			delim = strings.Index(v, "q=")
			if delim == -1 {
				add(1000, ctype)
				continue
			}
			v = v[delim+2:]
			delim = strings.Index(v, ";")
			if delim != -1 {
				v = v[:delim]
			}

			v = strings.TrimSpace(v)
			if len(v) == 0 || len(v) > 5 {
				return nil
			} else if v[0] == '1' {
				if len(v) != 1 {
					if v[1] != '.' {
						return nil
					}
					for _, c := range v[2:] {
						if c != '0' {
							return nil
						}
					}
				}
				add(1000, ctype)
			} else if v[0] == '0' {
				if len(v) == 1 {
					add(0, ctype)
				} else if len(v) == 2 {
					if v[1] == '.' {
						add(0, ctype)
					} else {
						return nil
					}
				} else {
					w, err := strconv.Atoi(v[2:])
					if err != nil {
						return nil
					}
					switch len(v[2:]) {
					case 1:
						w *= 100
					case 2:
						w *= 10
					}
					add(w, ctype)
				}
			} else {
				return nil
			}
		} else {
			add(1000, strings.TrimSpace(s))
		}
	}

	sort.Sort(sort.Reverse(sort.IntSlice(keys)))
	var out []string
	for _, k := range keys {
		out = append(out, parsed[k]...)
	}
	return out
}

func contentTypeEqual(value, category, subtype string) bool {
	delim := strings.Index(value, "/")
	if delim == -1 {
		return false
	}
	cat := value[:delim]
	sub := value[delim+1:]

	return (cat == "*" || cat == category) && (sub == "*" || sub == subtype)
}

func ReloadHTTP(cfg *Configuration) {
	if Config.HTTP.Listen != cfg.HTTP.Listen || Config.HTTP.ListenTLS != cfg.HTTP.ListenTLS {
		Log("HTTP: A server restart is required to change listening address")
	}
}

func StopHTTP() {
	if HTTPServer.Handler != nil {
		HTTPServer.Shutdown(context.Background())
		if HTTPSServer.Handler != nil {
			HTTPSServer.Shutdown(context.Background())
		}
	}
}

func StartHTTP(tlsConf *tls.Config) {
	var listenAddr string
	if tlsConf != nil {
		go StartHTTP(nil)

		listenAddr = Config.HTTP.ListenTLS
		if listenAddr == "" {
			listenAddr = ":443"
		}
		_, port, err := net.SplitHostPort(listenAddr)
		if err != nil {
			Logf("HTTP: Invalid address %s: %v", listenAddr, err)
			return
		}
		HTTPSPort = []byte(port)
	} else {
		listenAddr = Config.HTTP.Listen
		if listenAddr == "" {
			listenAddr = ":80"
		}
		_, port, err := net.SplitHostPort(listenAddr)
		if err != nil {
			Logf("HTTP: Invalid address %s: %v", listenAddr, err)
			return
		}
		HTTPPort = []byte(port)
	}

	var server *http.Server
	if tlsConf == nil {
		server = &HTTPServer
	} else {
		server = &HTTPSServer
	}
	server.ErrorLog = log.New(httpLog(0), "", 0)
	server.ReadTimeout = ReadTimeout
	server.WriteTimeout = WriteTimeout
	server.Handler = http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		ConfigLock.RLock()
		defer ConfigLock.RUnlock()
		Log("HTTP: ", req.RemoteAddr, " -> ", req.Method, " ", req.URL.Path)
		w.Header().Add("Tk", "N")
		w.Header().Add("Content-Security-Policy", "img-src 'self'; style-src 'self'; media-src 'self'; font-src 'self'; default-src 'none'; frame-ancestors 'none'; base-uri 'none'; form-action 'none'")
		w.Header().Add("Referrer-Policy", "no-referrer")
		w.Header().Add("X-XSS-Protection", "1")
		w.Header().Add("X-Frame-Options", "DENY")
		w.Header().Add("X-Content-Type-Options", "nosniff")
		w.Header().Add("Vary", "Accept")
		w.Header().Add("Accept-Ranges", "bytes")
		if tlsConf != nil {
			w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
		}

		if req.Method == "OPTIONS" {
			w.Header().Add("Allow", "GET, HEAD, OPTIONS")
			w.WriteHeader(204)
			return
		} else if req.Method != "GET" && req.Method != "HEAD" {
			w.Header().Add("Allow", "GET, HEAD, OPTIONS")
			w.WriteHeader(405)
			return
		}

		var host, port string
		if i := strings.LastIndex(req.Host, ":"); i != -1 {
			host = req.Host[:i]
			port = req.Host[i:]
		} else {
			host = req.Host
			port = ""
		}

		if host != Config.HTTP.Hostname {
			dest := *req.URL
			dest.Host = Config.HTTP.Hostname
			if tlsConf == nil {
				dest.Scheme = "http"
				if port != "" {
					dest.Host += port
				}
			} else {
				dest.Scheme = "https"
				if port != "" {
					dest.Host += port
				}
			}
			w.Header().Add("Location", dest.String())
			w.WriteHeader(308)
			return
		}

		resp := HandleClient(req.URL.Path, HTTPGenerator(0))

		var (
			layout []byte
			err    error
		)
		if Config.HTTP.Layout != "" {
			layout, err = ioutil.ReadFile(Config.HTTP.Layout)
			if err != nil {
				Log("HTTP: ", err)
				layout = nil
			}
		}

		ctype := resp.ContentType
		if i := strings.Index(resp.ContentType, ";"); i != -1 {
			ctype = resp.ContentType[:i]
		}
		if resp.Content != nil && layout != nil {
			if ctype == "text/html" {
				t := NewTemplate(resp.Content)
				port := HTTPPort
				if tlsConf != nil {
					port = HTTPSPort
				}
				m := StandardPlaceholders()
				m[HostPlaceholder] = []byte(Config.HTTP.Hostname)
				m[PortPlaceholder] = port
				m[ReqPathPlaceholder] = []byte(req.URL.Path)
				resp.Content = t.Apply(m)

				if layout != nil {
					t = NewTemplate(layout)
					m[LayoutContent] = resp.Content
					resp.Content = t.Apply(m)
				}
			}
		}

		if accept := req.Header.Get("Accept"); accept != "" {
			header := parseAccept(accept)
			if header == nil {
				w.WriteHeader(400)
				return
			}

			allowed := strings.Split(ctype, "/")
			if len(allowed) != 2 {
				panic("resp.ContentType is invalid: " + resp.ContentType)
			}
			ok := false
			for _, a := range header {
				if contentTypeEqual(a, allowed[0], allowed[1]) {
					ok = true
					break
				}
			}
			if !ok {
				w.WriteHeader(406)
				return
			}
		}

		enc := req.Header.Get("Accept-Encoding")
		if enc != "" {
		encLoop:
			for _, e := range parseAccept(enc) {
				var buf bytes.Buffer
				switch strings.TrimSpace(e) {
				case "gzip":
					writer := gzip.NewWriter(&buf)
					writer.Write(resp.Content)
					writer.Close()
					resp.Content = buf.Bytes()
					w.Header().Add("Content-Encoding", "gzip")
					break encLoop

				case "deflate":
					writer, _ := flate.NewWriter(&buf, 7)
					writer.Write(resp.Content)
					writer.Close()
					resp.Content = buf.Bytes()
					w.Header().Add("Content-Encoding", "deflate")
					break encLoop

				case "compress":
					writer := lzw.NewWriter(&buf, lzw.LSB, 8)
					writer.Write(resp.Content)
					writer.Close()
					resp.Content = buf.Bytes()
					w.Header().Add("Content-Encoding", "compress")
					break encLoop

				case "br":
					writer := brotli.NewWriter(&buf)
					writer.Write(resp.Content)
					writer.Close()
					resp.Content = buf.Bytes()
					w.Header().Add("Content-Encoding", "brotli")
					break encLoop
				}
			}
		}

		w.Header().Add("Content-Type", resp.ContentType)
		w.Header().Add("Content-Length", strconv.Itoa(len(resp.Content)))

		w.Header().Add("Cache-Control", "public; max-age=43200")
		switch resp.Status {
		case Ok:
			if resp.Content != nil {
				hash := md5.Sum(resp.Content)
				h := base64.URLEncoding.EncodeToString(hash[:])
				w.Header().Add("ETag", h)

				check := req.Header.Get("If-None-Match")
				if check == h {
					w.WriteHeader(304)
					return
				}

				r := req.Header.Get("Range")
				if strings.HasPrefix(r, "bytes=") {
					w.Header().Del("Content-Length")
					r = r[6:]
					var (
						ranges []Range
						total  int64
					)
					for _, v := range strings.Split(r, ",") {
						v = strings.TrimSpace(v)
						delim := strings.Index(v, "-")
						if delim == -1 {
							w.WriteHeader(400)
							return
						}

						var start, end int64
						if delim == 0 || delim == len(v)-1 {
							end = int64(len(resp.Content) - 1)
							if delim == 0 {
								v = v[1:]
							} else {
								v = v[:len(v)-1]
							}
							start, err = strconv.ParseInt(v, 10, 64)
							if err != nil {
								w.WriteHeader(400)
								return
							}
							start = int64(len(resp.Content)) - start
						} else {
							start, err = strconv.ParseInt(v[:delim], 10, 64)
							if err != nil {
								w.WriteHeader(400)
								return
							}
							end, err = strconv.ParseInt(v[delim+1:], 10, 64)
							if err != nil {
								w.WriteHeader(400)
								return
							}
						}

						if start < 0 || start >= int64(len(resp.Content)) || end < 0 || end >= int64(len(resp.Content)) || start > end {
							w.WriteHeader(416)
							return
						}
						ranges = append(ranges, Range{
							Start: start,
							End:   end,
						})
						total += end - start + 1
					}
					if len(ranges) == 1 {
						w.Header().Add("Content-Length", strconv.FormatInt(total, 10))
						w.Header().Add("Content-Range", fmt.Sprintf("bytes %d-%d/%d", ranges[0].Start, ranges[0].End, len(resp.Content)))
						w.WriteHeader(206)
						_, err = w.Write(resp.Content[ranges[0].Start : ranges[0].End+1])
						if err != nil {
							Log("HTTP: ", err)
							return
						}
					} else {
						// Don't support multiple ranges.
						w.WriteHeader(416)
					}
					return
				}
			}
			w.WriteHeader(200)

		case NotFound:
			w.WriteHeader(404)

		case Redirect:
			w.Header().Add("Location", string(resp.Redirect))
			w.WriteHeader(301)

		case Error:
			w.Header().Set("Cache-Control", "no-cache")
			w.Header().Add("Pragma", "no-cache")
			w.WriteHeader(500)
		}

		if req.Method != "HEAD" && resp.Content != nil {
			_, err := w.Write(resp.Content)
			if err != nil {
				Log("HTTP: ", err)
			}
		}
	})

	var err error
	server.Addr = listenAddr
	if tlsConf == nil {
		err = server.ListenAndServe()
	} else {
		server.TLSConfig = tlsConf

		// Disable HTTP2 to keep things simple
		server.TLSNextProto = map[string]func(*http.Server, *tls.Conn, http.Handler){}
		err = server.ListenAndServeTLS("", "")
	}
	if err != nil && err != http.ErrServerClosed {
		Log("HTTP: ", err)
	}
}
