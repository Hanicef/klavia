/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bytes"
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var GeminiPort []byte

var GeminiListener net.Listener
var IsGeminiStopping bool

var GeminiWait sync.WaitGroup

type GeminiGenerator int

func (g GeminiGenerator) FrontPage() *Response {
	data, err := ioutil.ReadFile(Config.Gemini.Pages.Front)
	if err != nil {
		Log("Gemini: Front page is missing")
		return g.Error()
	}

	data = bytes.Replace(data, []byte("\n"), []byte("\r\n"), -1)
	for _, c := range Categories {
		data = append(data, "=>"...)
		data = append(data, CategoryPrefix...)
		data = append(data, c.Name...)
		data = append(data, ' ')
		data = append(data, c.Title...)
		data = append(data, '\r', '\n')
	}
	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: "text/gemini; charset=" + Config.Document.Encoding,
	}
}

func (g GeminiGenerator) Category(cat string) *Response {
	if c, ok := Categories.Find(cat); ok {
		var data = []byte("# ")
		data = append(data, c.Title...)
		data = append(data, "\r\n\r\n"...)
		if c.Description != "" {
			data = append(data, c.Description...)
			data = append(data, "\r\n\r\n"...)
		}

		for _, d := range c.Documents {
			data = append(data, '=', '>')
			data = append(data, DocumentPrefix...)
			data = append(data, d.Name...)
			data = append(data, ' ')
			data = append(data, d.Title...)
			data = append(data, '\r', '\n', '=', '>')
			data = append(data, RawPrefix...)
			data = append(data, d.Name...)
			data = append(data, " [source]\r\n\r\n"...)
		}
		data = append(data, "=>/ <-- Go back\r\n"...)
		return &Response{
			Status:      Ok,
			Content:     data,
			ContentType: "text/gemini; charset=" + Config.Document.Encoding,
		}
	}

	return g.NotFound()
}

func (g GeminiGenerator) Document(file string, raw bool) *Response {
	doc, ok := Documents.Find(file)
	if !ok {
		return g.NotFound()
	}

	data, err := doc.Load()
	if err != nil {
		Logf("Gemini: Document %s is missing", doc.Name)
		return g.Error()
	}

	data = bytes.Replace(data, []byte("\n"), []byte("\r\n"), -1)
	ctype := "text/gemini; charset="
	if raw {
		ctype = "text/plain; charset="
	}
	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: ctype + Config.Document.Encoding,
	}
}

func (g GeminiGenerator) Resource(file string) *Response {
	res, ok := Config.Gemini.Resources[file]
	if !ok {
		res, ok = Config.Gemini.Directories.Retrieve(file)
		if !ok {
			return g.NotFound()
		}

		if stat, err := os.Stat(res); err != nil || stat.IsDir() {
			if err == nil || errors.Is(err, os.ErrNotExist) {
				return g.NotFound()
			}
			Logf("Gemini: Resource %s is inaccessible: %s", res, err)
			return g.Error()
		}
	}

	data, err := ioutil.ReadFile(res)
	if err != nil {
		Logf("Gemini: %s: Read error: %s", res, err)
		return g.Error()
	}

	ctype, err := GetDocumentType(res)
	if err != nil {
		Logf("Gemini: Cannot determine MIME type: %s", err)
		return g.Error()
	}

	if strings.HasPrefix(ctype, "text/") {
		data = bytes.Replace(data, []byte("\n"), []byte("\r\n"), -1)
	}

	return &Response{
		Status:      Ok,
		Content:     data,
		ContentType: ctype,
	}
}

func (g GeminiGenerator) NotFound() *Response {
	return &Response{
		Status:      NotFound,
		Content:     nil,
		ContentType: "",
	}
}

func (g GeminiGenerator) Error() *Response {
	return &Response{
		Status:      Error,
		Content:     nil,
		ContentType: "",
	}
}

func ReloadGemini(cfg *Configuration) {
	if Config.Gemini.Listen != cfg.Gemini.Listen {
		Log("Gemini: A server restart is required to change listening address")
	}
}

func StopGemini() {
	if GeminiListener != nil {
		IsGeminiStopping = true
		GeminiListener.Close()
		GeminiWait.Wait()
	}
}

func StartGemini(tlsConf *tls.Config) {
	if Config.Gemini.Listen == "" {
		Config.Gemini.Listen = ":1965"
	}
	var err error
	GeminiListener, err = tls.Listen("tcp", Config.Gemini.Listen, tlsConf)
	if err != nil {
		Log("Gemini: ", err)
		return
	}

	GeminiPort = []byte(strconv.Itoa(GeminiListener.Addr().(*net.TCPAddr).Port))

	for {
		conn, err := GeminiListener.Accept()
		if err != nil {
			if IsGeminiStopping {
				break
			}
			Log("Gemini: ", err)
			continue
		}

		GeminiWait.Add(1)
		go func(conn net.Conn) {
			defer conn.Close()
			ConfigLock.RLock()
			defer ConfigLock.RUnlock()
			defer GeminiWait.Done()

			err := conn.SetReadDeadline(time.Now().Add(ReadTimeout))
			if err != nil {
				Log("Gemini: ", err)
				return
			}

			line, err := ReadLine(conn)
			if err != nil {
				if neterr, ok := err.(net.Error); !ok || !neterr.Timeout() {
					Log("Gemini: ", err)
				}
				return
			}

			req, err := url.Parse(line)
			if err != nil {
				_, err = conn.Write([]byte("59 Invalid URL\r\n"))
				if err != nil {
					Log("Gemini: ", err)
				}
				return
			}

			var host, port string
			if i := strings.LastIndex(req.Host, ":"); i != -1 {
				host = req.Host[:i]
				port = req.Host[i:]
			} else {
				host = req.Host
				port = ""
			}

			if host != Config.Gemini.Hostname {
				req.Host = Config.Gemini.Hostname
				if port != "" {
					req.Host += port
				}
				_, err = conn.Write([]byte("31 " + req.String() + "\r\n"))
				if err != nil {
					Log("Gemini: ", err)
				}
				return
			}

			err = conn.SetWriteDeadline(time.Now().Add(WriteTimeout))
			if err != nil {
				Log("Gemini: ", err)
				return
			}

			if len(req.Path) == 0 {
				req.Path = "/"
			} else if req.Path[0] != '/' {
				_, err = conn.Write([]byte("59 Request must start with /\r\n"))
				if err != nil {
					Log("Gemini: ", err)
				}
				return
			}

			Log("Gemini: ", conn.RemoteAddr().String(), " -> ", req.Path)

			resp := HandleClient(req.Path, GeminiGenerator(0))
			switch resp.Status {
			case Ok:
				_, err = conn.Write([]byte("20 " + resp.ContentType + "\r\n"))
				if err != nil {
					Log("Gemini: ", err)
					return
				}
				if resp.Content != nil {
					t := NewTemplate(resp.Content)
					m := StandardPlaceholders()
					m[HostPlaceholder] = []byte(Config.Gemini.Hostname)
					m[PortPlaceholder] = GeminiPort
					m[ReqPathPlaceholder] = []byte(req.Path)
					resp.Content = t.Apply(m)
					_, err = conn.Write(resp.Content)
				}

			case NotFound:
				_, err = conn.Write([]byte("51 Nothing found at " + req.Path + "\r\n"))

			case Redirect:
				_, err = conn.Write([]byte("31 " + resp.Redirect + "\r\n"))

			case Error:
				_, err = conn.Write([]byte("40 A server error has occured\r\n"))
			}

			if err != nil {
				Log("Gemini: ", err)
				return
			}
		}(conn)
	}
}
