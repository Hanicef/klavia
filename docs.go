/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"
)

type Category struct {
	Name        string
	Title       string
	Description string
	Documents   []*Document
}

type Document struct {
	Name  string
	Title string
	Path  string
}

func (doc *Document) Load() ([]byte, error) {
	var out = append([]byte("# "), doc.Title...)
	out = append(out, '\n', '\n')
	if Config.Document.Header != "" {
		data, err := ioutil.ReadFile(Config.Document.Header)
		if err != nil {
			return nil, err
		}
		out = append(out, data...)
		out = append(out, '\n')
	}

	data, err := ioutil.ReadFile(doc.Path)
	if err != nil {
		return nil, err
	}
	return append(out, data...), nil
}

type DocumentList []Document

func (d DocumentList) Len() int {
	return len(d)
}

func (d DocumentList) Less(i, j int) bool {
	return d[i].Name < d[j].Name
}

func (d DocumentList) Swap(i, j int) {
	tmp := d[i]
	d[i] = d[j]
	d[j] = tmp
}

func (d DocumentList) Find(name string) (*Document, bool) {
	var (
		l = 0
		r = len(d) - 1
	)
	for l <= r {
		m := (l + r) / 2
		if d[m].Name < name {
			l = m + 1
		} else if d[m].Name > name {
			r = m - 1
		} else {
			return &d[m], true
		}
	}
	return nil, false
}

type CategoryList []Category

func (c CategoryList) Len() int {
	return len(c)
}

func (c CategoryList) Less(i, j int) bool {
	return c[i].Name < c[j].Name
}

func (c CategoryList) Swap(i, j int) {
	tmp := c[i]
	c[i] = c[j]
	c[j] = tmp
}

func (c CategoryList) Find(name string) (*Category, bool) {
	var (
		l = 0
		r = len(c) - 1
	)
	for l <= r {
		m := (l + r) / 2
		if c[m].Name < name {
			l = m + 1
		} else if c[m].Name > name {
			r = m - 1
		} else {
			return &c[m], true
		}
	}
	return nil, false
}

func readLine(reader *bufio.Reader) (string, error) {
	var (
		buf []byte
		v   []byte
		err error
	)
	isPrefix := true
	for isPrefix {
		v, isPrefix, err = reader.ReadLine()
		if err != nil {
			return "", err
		}
		buf = append(buf, v...)
	}

	line := string(buf)
	empty := true
	n := strings.IndexFunc(line, func(r rune) bool {
		if !unicode.IsSpace(r) && r != '#' {
			empty = false
		}
		return r == '#'
	})
	if empty {
		return "", nil
	}
	if n != -1 {
		line = line[:n]
	}
	return line, nil
}

func parseDocument(name string, reader *bufio.Reader, l *int, cats CategoryList) (Document, string, error) {
	doc := Document{
		Name: name,
	}
	for {
		(*l)++
		line, err := readLine(reader)
		if err != nil {
			return doc, "", err
		}
		if line == "" {
			continue
		}

		r, _ := utf8.DecodeRuneInString(line)
		if !unicode.IsSpace(r) {
			return doc, line, nil
		}

		k := strings.TrimSpace(line)
		i := strings.IndexFunc(k, unicode.IsSpace)
		if i == -1 {
			return doc, "", fmt.Errorf("line %d: value missing", *l)
		}
		v := strings.TrimSpace(k[i:])

		switch k[:i] {
		case "Path":
			if doc.Path != "" {
				return doc, "", fmt.Errorf("line %d: path already specified", *l)
			}
			doc.Path = v
		case "Title":
			if doc.Title != "" {
				return doc, "", fmt.Errorf("line %d: title already specified", *l)
			}
			doc.Title = v
		case "Category":
			ok := false
			for i, c := range cats {
				if c.Name == v {
					c.Documents = append(c.Documents, &doc)
					// Keep document ordered after title.
					for j := len(c.Documents) - 1; j > 0 && c.Documents[j-1].Title > c.Documents[j].Title; j-- {
						tmp := c.Documents[j]
						c.Documents[j] = c.Documents[j-1]
						c.Documents[j-1] = tmp
					}
					cats[i] = c
					ok = true
					break
				}
			}
			if !ok {
				return doc, "", fmt.Errorf("line %d: document %s not defined", *l, v)
			}
		default:
			return doc, "", fmt.Errorf("line %d: unknown key %s", *l, k)
		}
	}
}

func parseCategory(name string, reader *bufio.Reader, l *int) (Category, string, error) {
	cat := Category{
		Name: name,
	}
	for {
		(*l)++
		line, err := readLine(reader)
		if err != nil {
			return cat, "", err
		}
		if line == "" {
			continue
		}

		r, _ := utf8.DecodeRuneInString(line)
		if !unicode.IsSpace(r) {
			return cat, line, nil
		}

		k := strings.TrimSpace(line)
		i := strings.IndexFunc(k, unicode.IsSpace)
		if i == -1 {
			return cat, "", fmt.Errorf("line %d: value missing", *l)
		}
		v := strings.TrimSpace(k[i:])

		switch k[:i] {
		case "Title":
			cat.Title = v
		case "Description":
			cat.Description = v
		default:
			return cat, "", fmt.Errorf("line %d: unknown key %s", *l, k)
		}
	}
}

func loadDocuments(file io.Reader) (DocumentList, CategoryList, error) {
	reader := bufio.NewReader(file)
	docs := DocumentList{}
	cats := CategoryList{}

	l := 1
	var (
		line string
		err  error
	)
	for line == "" {
		line, err = readLine(reader)
		if err != nil {
			if err == io.EOF {
				return docs, cats, nil
			}
			return nil, nil, err
		}
	}

outerLoop:
	for {
		r, _ := utf8.DecodeRuneInString(line)
		if unicode.IsSpace(r) {
			return nil, nil, fmt.Errorf("line %d: missing block definition", l)
		}

		k := strings.TrimSpace(line)
		i := strings.IndexFunc(k, unicode.IsSpace)
		if i == -1 {
			return nil, nil, fmt.Errorf("line %d: value missing", l)
		}
		v := strings.TrimSpace(k[i:])

		switch k[:i] {
		case "Document":
			for i := range docs {
				if docs[i].Name == v {
					return nil, nil, fmt.Errorf("line %d: duplicate name %s", l, v)
				}
			}
			var doc Document
			doc, line, err = parseDocument(v, reader, &l, cats)
			docs = append(docs, doc)
			if err != nil {
				if err == io.EOF {
					break outerLoop
				}
				return nil, nil, err
			}

		case "Category":
			var cat Category
			cat, line, err = parseCategory(v, reader, &l)
			cats = append(cats, cat)
			if err != nil {
				if err == io.EOF {
					break outerLoop
				}
				return nil, nil, err
			}

		default:
			return nil, nil, fmt.Errorf("line %d: invalid category type", l)
		}
	}

	sort.Sort(docs)
	sort.Sort(cats)
	return docs, cats, nil
}

func LoadDocuments(table string) (DocumentList, CategoryList, error) {
	file, err := os.Open(table)
	if err != nil {
		return nil, nil, err
	}
	defer file.Close()
	docs, cats, err := loadDocuments(file)
	if err != nil {
		return nil, nil, err
	}
	return docs, cats, nil
}
