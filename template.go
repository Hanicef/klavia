/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"strconv"
	"time"
)

const (
	HostPlaceholder       = "HOST"
	PortPlaceholder       = "PORT"
	HTTPHostPlaceholder   = "HTTPHOST"
	HTTPPortPlaceholder   = "HTTPPORT"
	HTTPSPortPlaceholder  = "HTTPSPORT"
	GopherHostPlaceholder = "GOPHERHOST"
	GopherPortPlaceholder = "GOPHERPORT"
	GeminiHostPlaceholder = "GEMINIHOST"
	GeminiPortPlaceholder = "GEMINIPORT"
	ReqPathPlaceholder    = "REQPATH"
	DocCountPlaceholder   = "DOCCOUNT"
	CatCountPlaceholder   = "CATCOUNT"
	UptimePlaceholder     = "UPTIME"
	DatePlaceholder       = "DATE"
	TimePlaceholder       = "TIME"
	TimezonePlaceholder   = "TIMEZONE"
)

type Template struct {
	f  [][]byte // Text between placeholders
	ph []string // Placeholders
}

var startTime = time.Now()

func StandardPlaceholders() map[string][]byte {
	m := make(map[string][]byte, 10+len(Config.Placeholders))
	m[HTTPHostPlaceholder] = []byte(Config.HTTP.Hostname)
	m[HTTPPortPlaceholder] = HTTPPort
	m[HTTPSPortPlaceholder] = HTTPSPort
	m[GopherHostPlaceholder] = []byte(Config.Gopher.Hostname)
	m[GopherPortPlaceholder] = GopherPort
	m[GeminiHostPlaceholder] = []byte(Config.Gemini.Hostname)
	m[GeminiPortPlaceholder] = GeminiPort
	m[DocCountPlaceholder] = []byte(strconv.Itoa(len(Documents)))
	m[CatCountPlaceholder] = []byte(strconv.Itoa(len(Categories)))
	t := time.Now()
	m[UptimePlaceholder] = []byte(strconv.Itoa(int(t.Sub(startTime).Hours()) / 12))
	m[DatePlaceholder] = []byte(t.Format("2006-01-02"))
	m[TimePlaceholder] = []byte(t.Format("15:04:05"))
	z, _ := t.Zone()
	m[TimezonePlaceholder] = []byte(z)
	for k, v := range Config.Placeholders {
		m[k] = v
	}
	return m
}

func NewTemplate(v []byte) *Template {
	t := new(Template)
	n := 0
	for i := range v {
		if v[i] == '$' {
			t.f = append(t.f, v[n:i])
			n = i + 1
			for n < len(v) && (v[n] >= 'a' && v[n] <= 'z' || v[n] >= 'A' && v[n] <= 'Z') {
				n++
			}
			t.ph = append(t.ph, string(v[i+1:n]))
			i = n
		}
	}

	if n < len(v) {
		t.f = append(t.f, v[n:])
	}
	return t
}

func (t *Template) Apply(s map[string][]byte) []byte {
	var out []byte
	for i := range t.f {
		out = append(out, t.f[i]...)
		if i < len(t.ph) {
			if v, ok := s[t.ph[i]]; ok {
				out = append(out, v...)
			} else {
				out = append(out, '$')
				out = append(out, t.ph[i]...)
			}
		}
	}
	return out
}
