/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"encoding/xml"
	"errors"
	"flag"
	"io"
	"os"
	"path"
	"strings"
)

var ConfigFile = flag.String("config", "/etc/klavia.xml", "configuration file to use")

func parseTagList(d *xml.Decoder, tag string, f func(string, map[string]string) error) error {
	for {
		tok, err := d.Token()
		if err != nil {
			if err == io.EOF {
				return io.ErrUnexpectedEOF
			}
			return err
		}
		if start, ok := tok.(xml.StartElement); ok && start.Name.Local == tag {
			var attrib = make(map[string]string, len(start.Attr))
			for _, a := range start.Attr {
				attrib[a.Name.Local] = a.Value
			}

			tok, err = d.Token()
			if err != nil {
				if err == io.EOF {
					return io.ErrUnexpectedEOF
				}
				return err
			}

			if text, ok := tok.(xml.CharData); ok {
				err = f(string(text), attrib)
				if err != nil {
					return err
				}
				tok, err = d.Token()
				if err != nil {
					if err == io.EOF {
						return io.ErrUnexpectedEOF
					}
					return err
				}
			} else {
				return errors.New("missing resource name")
			}

			// xml package ensures that the end token matches the start token
			if _, ok := tok.(xml.EndElement); !ok {
				return errors.New("missing end token for " + tag)
			}
		} else if _, ok := tok.(xml.EndElement); ok {
			return nil
		}
	}
}

type CustomPlaceholder map[string][]byte

func (p *CustomPlaceholder) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	*p = make(CustomPlaceholder)
	return parseTagList(d, "Placeholder", func(c string, attrib map[string]string) error {
		key, ok := attrib["Key"]
		if !ok {
			return errors.New("Key attribute missing or empty")
		}
		(*p)[strings.ToUpper(key)] = []byte(c)
		return nil
	})
}

type ResourceMap map[string]string

func (r *ResourceMap) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	*r = make(ResourceMap)
	return parseTagList(d, "Resource", func(c string, attrib map[string]string) error {
		path, ok := attrib["Path"]
		if !ok {
			return errors.New("Path attribute missing or empty")
		}
		(*r)[path] = c
		return nil
	})
}

type DirectoryMap map[string]string

func (dir *DirectoryMap) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	*dir = make(DirectoryMap)
	return parseTagList(d, "Directory", func(c string, attrib map[string]string) error {
		path, ok := attrib["Path"]
		if !ok {
			return errors.New("Path attribute missing or empty")
		}
		(*dir)[path] = c
		return nil
	})
}

func (dir DirectoryMap) Retrieve(p string) (string, bool) {
	p = path.Clean(p)
	for k, v := range dir {
		if strings.HasPrefix(p, k) {
			return path.Join(v, p[len(k):]), true
		}
	}
	return "", false
}

type StandardPages struct {
	Front    string
	NotFound string
}

type Configuration struct {
	Document struct {
		Table    string
		Encoding string
		Header   string
	}
	TLS struct {
		Cert string
		Key  string
	}
	Gopher struct {
		Hostname    string
		Listen      string
		Pages       StandardPages
		Resources   ResourceMap
		Directories DirectoryMap
	}
	Gemini struct {
		Hostname    string
		Listen      string
		Pages       StandardPages
		Resources   ResourceMap
		Directories DirectoryMap
	}
	HTTP struct {
		Hostname    string
		Listen      string
		ListenTLS   string
		Layout      string
		Pages       StandardPages
		Resources   ResourceMap
		Directories DirectoryMap
	}
	Placeholders CustomPlaceholder
}

func ParseConfig() (*Configuration, error) {
	file, err := os.Open(*ConfigFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	config := new(Configuration)
	d := xml.NewDecoder(file)
	err = d.Decode(config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
