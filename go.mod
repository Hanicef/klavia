module gitlab.com/Hanicef/klavia

go 1.15

require golang.org/x/sys v0.1.0

require github.com/andybalholm/brotli v1.0.4
