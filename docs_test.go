/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"sort"
	"strings"
	"testing"
)

func AssertDocuments(t *testing.T, actual DocumentList, expect DocumentList) {
	if len(actual) != len(expect) {
		t.Errorf("len(actual) != %d (%d)", len(expect), len(actual))
		return
	}
	for i := range expect {
		if actual[i].Name != expect[i].Name {
			t.Errorf("actual[%d].Name != %s (%s)", i, expect[i].Name, actual[i].Name)
		}
		if actual[i].Title != expect[i].Title {
			t.Errorf("actual[%d].Title != %s (%s)", i, expect[i].Title, actual[i].Title)
		}
		if actual[i].Path != expect[i].Path {
			t.Errorf("actual[%d].Path != %s (%s)", i, expect[i].Path, actual[i].Path)
		}
	}
}

func TestDocumentList_Sort(t *testing.T) {
	docs := DocumentList{
		Document{
			Name:  "foo",
			Title: "Foo",
			Path:  "/foo",
		},
		Document{
			Name:  "bar",
			Title: "Bar",
			Path:  "/bar",
		},
		Document{
			Name:  "baz",
			Title: "Baz",
			Path:  "/baz",
		},
	}
	sortDocs := make(DocumentList, 3)
	copy(sortDocs, docs)
	sort.Sort(sortDocs)
	AssertDocuments(t, sortDocs, DocumentList{
		docs[1],
		docs[2],
		docs[0],
	})
}

func TestDocumentList_Find(t *testing.T) {
	docs := DocumentList{
		Document{
			Name:  "bar",
			Title: "Bar",
			Path:  "/bar",
		},
		Document{
			Name:  "baz",
			Title: "Baz",
			Path:  "/baz",
		},
		Document{
			Name:  "foo",
			Title: "Foo",
			Path:  "/foo",
		},
	}
	doc, ok := docs.Find("bar")
	if !ok {
		t.Errorf("docs.Find(%s) != ok", "bar")
	} else if *doc != docs[0] {
		t.Errorf("docs.Find(%s) != %s (%s)", "bar", docs[0].Name, doc.Name)
	}

	doc, ok = docs.Find("baz")
	if !ok {
		t.Errorf("docs.Find(%s) != ok", "baz")
	} else if *doc != docs[1] {
		t.Errorf("docs.Find(%s) != %s (%s)", "baz", docs[1].Name, doc.Name)
	}

	doc, ok = docs.Find("foo")
	if !ok {
		t.Errorf("docs.Find(%s) != ok", "foo")
	} else if *doc != docs[2] {
		t.Errorf("docs.Find(%s) != %s (%s)", "foo", docs[2].Name, doc.Name)
	}
}

func AssertCategories(t *testing.T, actual CategoryList, expect CategoryList) {
	if len(actual) != len(expect) {
		t.Errorf("len(actual) != %d (%d)", len(expect), len(actual))
		return
	}
	for i := range expect {
		if actual[i].Name != expect[i].Name {
			t.Errorf("actual[%d].Name != %s (%s)", i, expect[i].Name, actual[i].Name)
		}
		if actual[i].Title != expect[i].Title {
			t.Errorf("actual[%d].Title != %s (%s)", i, expect[i].Title, actual[i].Title)
		}
		if len(actual[i].Documents) != len(expect[i].Documents) {
			t.Errorf("len(actual[%d].Documents) != %d (%d)", i, len(expect[i].Documents), len(actual[i].Documents))
		} else {
			actualDocs := make(DocumentList, len(actual[i].Documents))
			for j, d := range actual[i].Documents {
				actualDocs[j] = *d
			}
			expectDocs := make(DocumentList, len(expect[i].Documents))
			for j, d := range expect[i].Documents {
				expectDocs[j] = *d
			}
			AssertDocuments(t, actualDocs, expectDocs)
		}
	}
}

func TestCategoryList_Sort(t *testing.T) {
	cats := CategoryList{
		Category{
			Name:  "foo",
			Title: "Foo",
		},
		Category{
			Name:  "bar",
			Title: "Bar",
		},
		Category{
			Name:  "baz",
			Title: "Baz",
		},
	}
	sortCats := make(CategoryList, 3)
	copy(sortCats, cats)
	sort.Sort(sortCats)
	AssertCategories(t, sortCats, CategoryList{
		cats[1],
		cats[2],
		cats[0],
	})
}

func TestCategoryList_Find(t *testing.T) {
	cats := CategoryList{
		Category{
			Name:  "bar",
			Title: "Bar",
		},
		Category{
			Name:  "baz",
			Title: "Baz",
		},
		Category{
			Name:  "foo",
			Title: "Foo",
		},
	}
	cat, ok := cats.Find("bar")
	if !ok {
		t.Errorf("cats.Find(%s) != ok", "bar")
	} else if cat.Name != cats[0].Name {
		t.Errorf("cats.Find(%s) != %s (%s)", "bar", cats[0].Name, cat.Name)
	}

	cat, ok = cats.Find("baz")
	if !ok {
		t.Errorf("cats.Find(%s) != ok", "baz")
	} else if cat.Name != cats[1].Name {
		t.Errorf("cats.Find(%s) != %s (%s)", "baz", cats[1].Name, cat.Name)
	}

	cat, ok = cats.Find("foo")
	if !ok {
		t.Errorf("cats.Find(%s) != ok", "foo")
	} else if cat.Name != cats[2].Name {
		t.Errorf("cats.Find(%s) != %s (%s)", "foo", cats[2].Name, cat.Name)
	}
}

const Example = `
# Comment
Document example
	Title Example
	Path /foo/bar
`

const Empty = `# Nothing
`

const Multiple = `
Document foo # example
	Path /foo/bar
	Title Foo bar

Document bar
  Path /lorem/ipsum # example

Document space

  # Space
  Title Space bar
`

const SingleCategory = `
Category a
	Title A

Document foo
	Path /foo/bar
	Category a

Document bar
	Path /lorem/ipsum
	Category a
`

const MultipleCategories = `
Category a
	Title A

Category b
	Title B

Document foo
	Path /foo/bar
	Category b

Document bar
	Path /lorem/ipsum
	Category a

Document baz
	Path /a/b
	Category a
	Category b
`

const LargeData = `
Category lorem
	Title Lorem

Category ipsum
	Title Ipsum

Category dolor
	Title Dolor

Category sit
	Title Sit

Document a
	Path /foo
	Title Foo
	Category lorem

Document b
	Path /bar
	Title Bar
	Category ipsum

Document c
	Path /baz
	Title Baz
	Category lorem

Document d
	Path /foo/bar
	Title Foo Bar
	Category dolor

Document e
	Path /bar/bar
	Title Bar Bar
	Category dolor

Document f
	Path /bar/foo
	Title Bar Foo
	Category sit
`

const NoName = `
  Title Invalid
  Path /not/valid
`

const NoType = `
foo
	Title foo
`

const InvalidType = `
Invalid foo
	Title foo
`

const NoValue = `
Document foo
	Path
`

const UnknownKey = `
Document foo
	Unknown value
`

const MultipleTitles = `
Document foo
	Title Foo
	Title Bar
`

const MultiplePaths = `
Document foo
	Path /foo
	Path /bar
`

const DuplicateNames = `
Document foo
	Title Foo

Document foo
	Title Bar
`

func TestLoadDocuments(t *testing.T) {
	r := strings.NewReader(Example)
	docs, cats, err := loadDocuments(r)
	if err != nil {
		t.Errorf("err != nil (%s)", err)
	} else {
		AssertDocuments(t, docs, DocumentList{
			Document{
				Name:  "example",
				Title: "Example",
				Path:  "/foo/bar",
			},
		})
		AssertCategories(t, cats, CategoryList{})
	}

	r = strings.NewReader(Empty)
	docs, cats, err = loadDocuments(r)
	if err != nil {
		t.Errorf("err != nil (%s)", err)
	} else {
		AssertDocuments(t, docs, DocumentList{})
		AssertCategories(t, cats, CategoryList{})
	}

	r = strings.NewReader(Multiple)
	docs, cats, err = loadDocuments(r)
	if err != nil {
		t.Errorf("err != nil (%s)", err)
	} else {
		AssertDocuments(t, docs, DocumentList{
			Document{
				Name: "bar",
				Path: "/lorem/ipsum",
			},
			Document{
				Name:  "foo",
				Title: "Foo bar",
				Path:  "/foo/bar",
			},
			Document{
				Name:  "space",
				Title: "Space bar",
			},
		})
		AssertCategories(t, cats, CategoryList{})
	}

	r = strings.NewReader(SingleCategory)
	docs, cats, err = loadDocuments(r)
	if err != nil {
		t.Errorf("err != nil (%s)", err)
	} else {
		AssertDocuments(t, docs, DocumentList{
			Document{
				Name: "bar",
				Path: "/lorem/ipsum",
			},
			Document{
				Name: "foo",
				Path: "/foo/bar",
			},
		})
		AssertCategories(t, cats, CategoryList{
			Category{
				Name:  "a",
				Title: "A",
				Documents: []*Document{
					&Document{
						Name: "foo",
						Path: "/foo/bar",
					},
					&Document{
						Name: "bar",
						Path: "/lorem/ipsum",
					},
				},
			},
		})
	}

	r = strings.NewReader(MultipleCategories)
	docs, cats, err = loadDocuments(r)
	if err != nil {
		t.Errorf("err != nil (%s)", err)
	} else {
		AssertDocuments(t, docs, DocumentList{
			Document{
				Name: "bar",
				Path: "/lorem/ipsum",
			},
			Document{
				Name: "baz",
				Path: "/a/b",
			},
			Document{
				Name: "foo",
				Path: "/foo/bar",
			},
		})
		AssertCategories(t, cats, CategoryList{
			Category{
				Title: "A",
				Name:  "a",
				Documents: []*Document{
					&Document{
						Name: "bar",
						Path: "/lorem/ipsum",
					},
					&Document{
						Name: "baz",
						Path: "/a/b",
					},
				},
			},
			Category{
				Title: "B",
				Name:  "b",
				Documents: []*Document{
					&Document{
						Name: "foo",
						Path: "/foo/bar",
					},
					&Document{
						Name: "baz",
						Path: "/a/b",
					},
				},
			},
		})
	}

	r = strings.NewReader(LargeData)
	docs, cats, err = loadDocuments(r)
	if err != nil {
		t.Errorf("err != nil (%s)", err)
	} else {
		AssertDocuments(t, docs, DocumentList{
			Document{
				Name:  "a",
				Path:  "/foo",
				Title: "Foo",
			},
			Document{
				Name:  "b",
				Path:  "/bar",
				Title: "Bar",
			},
			Document{
				Name:  "c",
				Path:  "/baz",
				Title: "Baz",
			},
			Document{
				Name:  "d",
				Path:  "/foo/bar",
				Title: "Foo Bar",
			},
			Document{
				Name:  "e",
				Path:  "/bar/bar",
				Title: "Bar Bar",
			},
			Document{
				Name:  "f",
				Path:  "/bar/foo",
				Title: "Bar Foo",
			},
		})
		AssertCategories(t, cats, CategoryList{
			Category{
				Name:  "dolor",
				Title: "Dolor",
				Documents: []*Document{
					&Document{
						Name:  "e",
						Path:  "/bar/bar",
						Title: "Bar Bar",
					},
					&Document{
						Name:  "d",
						Path:  "/foo/bar",
						Title: "Foo Bar",
					},
				},
			},
			Category{
				Name:  "ipsum",
				Title: "Ipsum",
				Documents: []*Document{
					&Document{
						Name:  "b",
						Path:  "/bar",
						Title: "Bar",
					},
				},
			},
			Category{
				Name:  "lorem",
				Title: "Lorem",
				Documents: []*Document{
					&Document{
						Name:  "c",
						Path:  "/baz",
						Title: "Baz",
					},
					&Document{
						Name:  "a",
						Path:  "/foo",
						Title: "Foo",
					},
				},
			},
			Category{
				Name:  "sit",
				Title: "Sit",
				Documents: []*Document{
					&Document{
						Name:  "f",
						Path:  "/bar/foo",
						Title: "Bar Foo",
					},
				},
			},
		})
	}

	r = strings.NewReader(NoName)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}

	r = strings.NewReader(NoType)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}

	r = strings.NewReader(InvalidType)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}

	r = strings.NewReader(NoValue)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}

	r = strings.NewReader(UnknownKey)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}

	r = strings.NewReader(MultipleTitles)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}

	r = strings.NewReader(MultiplePaths)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}

	r = strings.NewReader(DuplicateNames)
	_, _, err = loadDocuments(r)
	if err == nil {
		t.Error("err == nil")
	}
}
