/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"encoding/xml"
	"strings"
	"testing"
)

func TestCustomPlaceholder_UnmarshalXML(t *testing.T) {
	var testResource = func(actual, expect CustomPlaceholder) {
		if len(actual) != len(expect) {
			t.Errorf("len(actual) != %d (%d)", len(expect), len(actual))
		} else {
			for k, v := range expect {
				val, ok := actual[k]
				if !ok {
					t.Errorf("%s is not in actual", string(k))
				} else if string(val) != string(v) {
					t.Errorf("actual[k] != %s (%s)", string(v), string(val))
				}
			}
		}
	}

	d := xml.NewDecoder(strings.NewReader("<Placeholders></Placeholders>"))
	tok, _ := d.Token()

	var p CustomPlaceholder
	err := p.UnmarshalXML(d, tok.(xml.StartElement))
	if err != nil {
		t.Error("err:", err)
	} else {
		testResource(p, CustomPlaceholder{})
	}

	d = xml.NewDecoder(strings.NewReader(`<Placeholders>
		<Placeholder Key="FOO">foo</Placeholder>
	</Placeholders>`))
	tok, _ = d.Token()

	err = p.UnmarshalXML(d, tok.(xml.StartElement))
	if err != nil {
		t.Error("err:", err)
	} else {
		testResource(p, CustomPlaceholder{
			"FOO": []byte("foo"),
		})
	}

	d = xml.NewDecoder(strings.NewReader(`<Placeholders>
		<Placeholder Key="foo">foo</Placeholder>
		<Placeholder Key="bar">bar</Placeholder>
	</Placeholders>`))
	tok, _ = d.Token()

	err = p.UnmarshalXML(d, tok.(xml.StartElement))
	if err != nil {
		t.Error("err:", err)
	} else {
		testResource(p, CustomPlaceholder{
			"FOO": []byte("foo"),
			"BAR": []byte("bar"),
		})
	}

	d = xml.NewDecoder(strings.NewReader(`<Placeholders>
		<Placeholder Key="foo">foo</Placeholder>
		<Placeholder>error</Placeholder>
	</Placeholders>`))
	tok, _ = d.Token()

	err = p.UnmarshalXML(d, tok.(xml.StartElement))
	if err == nil {
		t.Error("should error")
	}

	d = xml.NewDecoder(strings.NewReader(`<Placeholders>
		<Placeholder Key="foo">foo</Placeholder>
		<Placeholder Key="error"></Placeholder>
	</Placeholders>`))
	tok, _ = d.Token()

	err = p.UnmarshalXML(d, tok.(xml.StartElement))
	if err == nil {
		t.Error("should error")
	}
}

func TestResourceMap_UnmarshalXML(t *testing.T) {
	var testResource = func(actual, expect ResourceMap) {
		if len(actual) != len(expect) {
			t.Error("len(actual) !=", len(expect))
		} else {
			for k, v := range expect {
				val, ok := actual[k]
				if !ok || val != v {
					t.Error("actual[k] !=", v)
				}
			}
		}
	}

	d := xml.NewDecoder(strings.NewReader("<Resources></Resources>"))
	tok, _ := d.Token()

	var r ResourceMap
	err := r.UnmarshalXML(d, tok.(xml.StartElement))
	if err != nil {
		t.Error("err:", err)
	} else {
		testResource(r, ResourceMap{})
	}

	d = xml.NewDecoder(strings.NewReader(`<Resources>
		<Resource Path="test">Test</Resource>
	</Resources>`))
	tok, _ = d.Token()

	err = r.UnmarshalXML(d, tok.(xml.StartElement))
	if err != nil {
		t.Error("err:", err)
	} else {
		testResource(r, ResourceMap{
			"test": "Test",
		})
	}

	d = xml.NewDecoder(strings.NewReader(`<Resources>
		<Resource Path="foo">Foo</Resource>
		<Resource Path="bar">Bar</Resource>
	</Resources>`))
	tok, _ = d.Token()

	err = r.UnmarshalXML(d, tok.(xml.StartElement))
	if err != nil {
		t.Error("err:", err)
	} else {
		testResource(r, ResourceMap{
			"foo": "Foo",
			"bar": "Bar",
		})
	}

	d = xml.NewDecoder(strings.NewReader(`<Resources>
		<Resource Path="foo">Foo</Resource>
		<Resource>Error</Resource>
	</Resources>`))
	tok, _ = d.Token()

	err = r.UnmarshalXML(d, tok.(xml.StartElement))
	if err == nil {
		t.Error("should error")
	}

	d = xml.NewDecoder(strings.NewReader(`<Resources>
		<Resource Path="foo">Foo</Resource>
		<Resource Path="path"></Resource>
	</Resources>`))
	tok, _ = d.Token()

	err = r.UnmarshalXML(d, tok.(xml.StartElement))
	if err == nil {
		t.Error("should error")
	}
}

func TestDirectoryMap_Retrieve(t *testing.T) {
	dm := DirectoryMap{
		"/foo/bar": "/foo",
		"/lorem":   "/foo/bar",
	}

	s, ok := dm.Retrieve("/foo/bar/baz")
	if !ok {
		t.Fail()
	} else if s != "/foo/baz" {
		t.Fail()
	}

	s, ok = dm.Retrieve("/lorem/baz")
	if !ok {
		t.Fail()
	} else if s != "/foo/bar/baz" {
		t.Fail()
	}

	s, ok = dm.Retrieve("/lorem/ipsum/../baz")
	if !ok {
		t.Error("/lorem/ipsum/../baz != ok")
	} else if s != "/foo/bar/baz" {
		t.Errorf("s != \"/foo/bar/baz\" (%s)", s)
	}

	s, ok = dm.Retrieve("/lorem/../lorem/ipsum")
	if !ok {
		t.Error("/lorem/../lorem/ipsum != ok")
	} else if s != "/foo/bar/ipsum" {
		t.Errorf("s != \"/foo/bar/ipsum\" (%s)", s)
	}

	s, ok = dm.Retrieve("/lorem/../baz")
	if ok {
		t.Fail()
	}

	s, ok = dm.Retrieve("/aeiou")
	if ok {
		t.Fail()
	}
}
