/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"testing"
)

func TestConvertGeminiToHTML(t *testing.T) {
	var input = `# Title

## Subtitle

Content.
More content.

=> http://example.com/
=> http://example.com/ A link

` + "```" + `
Preformatted
  1 2 3
` + "```" + `

List:
* 1
* 2
* 3

> Quote
> foo
> bar
`

	var output = "<h1>Title</h1>" +
		"<br/>" +
		"<h2>Subtitle</h2>" +
		"<br/>" +
		"Content.<br/>" +
		"More content.<br/>" +
		"<br/>" +
		"<a href=\"http://example.com/\">http://example.com/</a><br/>" +
		"<a href=\"http://example.com/\">A link</a><br/>" +
		"<br/>" +
		"<pre>Preformatted\r\n" +
		"  1 2 3\r\n" +
		"</pre><br/>" +
		"List:<br/>" +
		"<ul>" +
		"<li>1</li>" +
		"<li>2</li>" +
		"<li>3</li>" +
		"</ul>" +
		"<br/>" +
		"<blockquote>Quote<br/>" +
		"foo<br/>" +
		"bar<br/>" +
		"</blockquote>"

	data := ConvertGeminiToHTML([]byte(input))
	if string(data) != output {
		t.Log("Output:")
		t.Log(string(data))
		t.Fail()
	}
}

func TestParseAccept(t *testing.T) {
	var assertAccept = func(actual, expect []string) {
		if len(actual) != len(expect) {
			t.Errorf("len(actual) != %d (%d)", len(expect), len(actual))
			return
		}
		for i := range expect {
			if actual[i] != expect[i] {
				t.Errorf("actual[%d] != %s (%s)", i, expect[i], actual[i])
			}
		}
	}
	accept := "text/plain, application/json"
	values := parseAccept(accept)
	assertAccept(values, []string{"text/plain", "application/json"})

	accept = "text/plain;q=0.8, application/json"
	values = parseAccept(accept)
	assertAccept(values, []string{"application/json", "text/plain"})

	accept = "text/plain;q=0.8, text/html;q=0.8, application/json"
	values = parseAccept(accept)
	assertAccept(values, []string{"application/json", "text/plain", "text/html"})

	accept = "text/plain; q=0.8, text/html ; q=0.6"
	values = parseAccept(accept)
	assertAccept(values, []string{"text/plain", "text/html"})

	accept = "text/plain; q=0.25, text/html ; q=0.5"
	values = parseAccept(accept)
	assertAccept(values, []string{"text/html", "text/plain"})

	accept = "text/plain; q=0.25, text/html ; q=0.5"
	values = parseAccept(accept)
	assertAccept(values, []string{"text/html", "text/plain"})

	accept = "text/plain; q=0, text/html ; q=0."
	values = parseAccept(accept)
	assertAccept(values, []string{"text/plain", "text/html"})

	accept = "text/plain; q=1., text/html; q=1, application/json"
	values = parseAccept(accept)
	assertAccept(values, []string{"text/plain", "text/html", "application/json"})

	accept = "text/plain; q=0.1000"
	values = parseAccept(accept)
	if values != nil {
		t.Error("values != nil")
	}

	accept = "text/plain; q=20"
	values = parseAccept(accept)
	if values != nil {
		t.Error("values != nil")
	}

	accept = "text/plain; q=14"
	values = parseAccept(accept)
	if values != nil {
		t.Error("values != nil")
	}

	accept = "text/plain; q=04"
	values = parseAccept(accept)
	if values != nil {
		t.Error("values != nil")
	}

	accept = "text/plain; q=4"
	values = parseAccept(accept)
	if values != nil {
		t.Error("values != nil")
	}

	accept = "text/plain; q=1.5"
	values = parseAccept(accept)
	if values != nil {
		t.Error("values != nil")
	}

	accept = "text/plain; q="
	values = parseAccept(accept)
	if values != nil {
		t.Error("values != nil")
	}
}

func TestContentTypeEqual(t *testing.T) {
	if !contentTypeEqual("text/html", "text", "html") {
		t.Error("text/html")
	}

	if !contentTypeEqual("*/html", "text", "html") {
		t.Error("*/html")
	}

	if !contentTypeEqual("text/*", "text", "html") {
		t.Error("text/*")
	}

	if !contentTypeEqual("*/*", "text", "html") {
		t.Error("*/*")
	}

	if contentTypeEqual("application/json", "text", "html") {
		t.Error("application/json")
	}

	if contentTypeEqual("*/json", "text", "html") {
		t.Error("*/json")
	}

	if contentTypeEqual("application/*", "text", "html") {
		t.Error("application/*")
	}
}
