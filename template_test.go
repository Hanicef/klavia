/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import "testing"

func TestTemplate(t *testing.T) {
	var testApply = func(actual []byte, expect string) {
		if string(actual) != expect {
			t.Errorf("\"%s\" != \"%s\"", string(actual), expect)
		}
	}
	tp := NewTemplate([]byte("hello, $NAME!"))
	s := tp.Apply(map[string][]byte{"NAME": []byte("world")})
	testApply(s, "hello, world!")

	tp = NewTemplate([]byte("$A $B $C"))
	s = tp.Apply(map[string][]byte{
		"A": []byte("foo"),
		"B": []byte("bar"),
		"C": []byte("baz"),
	})
	testApply(s, "foo bar baz")

	tp = NewTemplate([]byte("$FOO bar $BAZ"))
	s = tp.Apply(map[string][]byte{
		"FOO": []byte("foo"),
		"BAZ": []byte("baz"),
	})
	testApply(s, "foo bar baz")

	tp = NewTemplate([]byte("$A $B $C"))
	s = tp.Apply(map[string][]byte{
		"A": []byte("foo"),
		"C": []byte("baz"),
	})
	testApply(s, "foo $B baz")
}
