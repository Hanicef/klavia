/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"golang.org/x/sys/unix"

	"crypto/tls"
	"errors"
	"flag"
	"os"
	"os/signal"
	"strconv"
	"sync"
)

var Documents DocumentList
var Categories CategoryList
var Config *Configuration

var ConfigLock sync.RWMutex

var daemon = flag.Bool("daemon", false, "fork the process to the background")
var pidFile = flag.String("pidfile", "", "file to dump pid to")

func exit(code int) {
	if *pidFile != "" {
		err := os.Remove(*pidFile)
		if err != nil {
			Log("Failed to remove PID file: ", err)
			Log("Consider removing it manually at ", *pidFile)
		}
	}
	os.Exit(code)
}

func daemonize() error {
	n, _, errno := unix.Syscall(unix.SYS_FORK, 0, 0, 0)
	if errno != 0 {
		Log("Failed to fork: ", unix.ErrnoName(errno))
		os.Exit(1)
	}
	if n != 0 {
		os.Exit(0)
	}

	_, err := unix.Setsid()
	if err != nil {
		Log("Failed to create a session: ", err)
		os.Exit(1)
	}

	n, _, errno = unix.Syscall(unix.SYS_FORK, 0, 0, 0)
	if errno != 0 {
		Log("Failed to fork: ", unix.ErrnoName(errno))
		os.Exit(1)
	}
	if n != 0 {
		os.Exit(0)
	}

	nullfd, err := os.OpenFile("/dev/null", os.O_RDWR, 0)
	if err != nil {
		Log("Failed to open null file: ", err)
		os.Exit(1)
	}
	os.Stdin.Close()
	os.Stdin = nullfd
	os.Stdout.Close()
	os.Stdout = nullfd
	os.Stderr.Close()
	os.Stderr = nullfd

	exePath := os.Args[0]
	if exePath[0] != '/' {
		workdir, err := os.Getwd()
		if err != nil {
			Log("Failed to get working directory: ", err)
			os.Exit(1)
		}

		exePath = workdir + "/" + exePath
	}

	unix.Umask(0)
	err = os.Chdir("/")
	if err != nil {
		Log("Failed to change to root directory: ", err)
		os.Exit(1)
	}

	args := make([]string, 0, len(os.Args))
	for _, arg := range os.Args {
		if arg != "-daemon" {
			args = append(args, arg)
		}
	}

	// Due to Go's sensitive scheduler, we have to re-exec the program to avoid
	// breaking it.
	err = unix.Exec(exePath, args, os.Environ())
	Log("Failed to execute process: ", err)
	os.Exit(1)
	return nil
}

func writePid(file string) {
	pid := []byte(strconv.Itoa(os.Getpid()))

	fd, err := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0644)
	if err != nil {
		if errors.Is(err, os.ErrExist) {
			Log("PID file already exists! Please check so the server is not already")
			Log("running, and shut it down if so. Otherwise, just remove the PID file.")
		}
		Log("Failed to open PID file: ", err)
		os.Exit(1)
	}
	defer fd.Close()

	_, err = fd.Write(pid)
	if err != nil {
		Log("Failed to write to PID file: ", err)
		exit(1)
	}
}

func loadCertificate(client *tls.ClientHelloInfo) (*tls.Certificate, error) {
	cert, err := tls.LoadX509KeyPair(Config.TLS.Cert, Config.TLS.Key)
	if err != nil {
		Log("Failed to load certificate: ", err)
		return nil, err
	}
	return &cert, nil
}

func main() {
	flag.Parse()

	if *daemon {
		daemonize()
	}

	if *pidFile != "" {
		writePid(*pidFile)
	}

	var err error
	Config, err = ParseConfig()
	if err != nil {
		Log("Failed to load configuration: ", err)
		exit(1)
	}

	if Config.Document.Table == "" {
		Config.Document.Table = "/etc/klavia/docs"
	}
	if Config.Document.Encoding == "" {
		Config.Document.Encoding = "utf-8"
	}
	Documents, Categories, err = LoadDocuments(Config.Document.Table)
	if err != nil {
		Log("Failed to load document table: ", err)
		exit(1)
	}

	var tlsConf *tls.Config
	if Config.TLS.Cert != "" && Config.TLS.Key != "" {
		tlsConf = new(tls.Config)
		tlsConf.GetCertificate = loadCertificate
		tlsConf.MinVersion = tls.VersionTLS12
	}

	if Config.HTTP.Hostname != "" {
		go StartHTTP(tlsConf)
	}
	if Config.Gopher.Hostname != "" {
		go StartGopher()
	}
	if Config.Gemini.Hostname != "" && tlsConf != nil {
		go StartGemini(tlsConf)
	}

	handler := make(chan os.Signal, 1)
	signal.Notify(handler, unix.SIGUSR1, unix.SIGTERM)
	for {
		signal := <-handler
		switch signal {
		case unix.SIGUSR1:
			Log("Caught SIGUSR1; reloading configuration")
			ConfigLock.Lock()

			cfg, err := ParseConfig()
			if err != nil {
				Log("Failed to load configuration: ", err)
				Log("Reverting back to old configuration")
			} else {
				docs, cats, err := LoadDocuments(cfg.Document.Table)
				if err != nil {
					Log("Failed to load document table: ", err)
					Log("Reverting back to old configuration")
				} else {
					ReloadGopher(cfg)
					ReloadGemini(cfg)
					ReloadHTTP(cfg)
					Config = cfg
					Documents = docs
					Categories = cats
				}
			}
			ConfigLock.Unlock()

		case unix.SIGTERM:
			Log("Caught SIGTERM; shutting down")
			StopGopher()
			StopGemini()
			StopHTTP()
			exit(0)
		}
	}
}
