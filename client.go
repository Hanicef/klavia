/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"strings"
)

const (
	Ok = iota
	NotFound
	Redirect
	Error
)

const (
	NamePlaceholder = "NAME"
	PathPlaceholder = "PATH"
)

const (
	DocumentPrefix = "/document/"
	RawPrefix      = "/document/raw/"
	CategoryPrefix = "/category/"
)

type Response struct {
	Status      int
	Content     []byte
	ContentType string // Ignored when using Gopher
	Redirect    string // Only used for Redirect response
}

type Generator interface {
	FrontPage() *Response
	Category(string) *Response
	Document(string, bool) *Response
	Resource(string) *Response
}

func HandleClient(path string, gen Generator) *Response {
	if path == "/" {
		return gen.FrontPage()
	} else if strings.HasPrefix(path, RawPrefix) {
		return gen.Document(path[len(RawPrefix):], true)
	} else if strings.HasPrefix(path, DocumentPrefix) {
		if strings.HasSuffix(path, ".txt") {
			// Redirect to avoid breaking old links.
			path = RawPrefix + path[len(DocumentPrefix):len(path)-4]
			return &Response{
				Status:      Redirect,
				Content:     []byte("Redirecting to " + path),
				ContentType: "text/plain",
				Redirect:    path,
			}
		}
		return gen.Document(path[len(DocumentPrefix):], false)
	} else if strings.HasPrefix(path, CategoryPrefix) {
		return gen.Category(path[len(CategoryPrefix):])
	} else {
		return gen.Resource(path)
	}
}
