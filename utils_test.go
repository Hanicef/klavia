/*
	This file is part of Klavia.

	Klavia is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Klavia is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Klavia.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"io"
	"strings"
	"testing"
)

func TestReadLine(t *testing.T) {
	s := strings.NewReader("hello\r\n")
	l, err := ReadLine(s)
	if err != nil {
		t.Error("err != nil")
	} else if l != "hello" {
		t.Error("l != \"hello\"")
	}

	l, err = ReadLine(s)
	if err != io.EOF {
		t.Error("err != io.EOF")
	}

	s = strings.NewReader("valid\r\ninvalid\nvalid\r\n")
	l, err = ReadLine(s)
	if err != nil {
		t.Error("err != nil")
	} else if l != "valid" {
		t.Error("l != \"valid\"")
	}

	l, err = ReadLine(s)
	if err != nil {
		t.Error("err != nil")
	} else if l != "invalid\nvalid" {
		t.Error("l != \"invalid\\nvalid\"")
	}

	l, err = ReadLine(s)
	if err != io.EOF {
		t.Error("err != io.EOF")
	}

	s = strings.NewReader("/docs\r\n")
	l, err = ReadLine(s)
	if err != nil {
		t.Error("err != nil")
	} else if l != "/docs" {
		t.Error("l != \"/docs\"")
	}

	var b []byte
	for i := 0; i < MaxLineLength+1; i++ {
		b = append(b, ' ')
	}
	s = strings.NewReader(string(b))
	l, err = ReadLine(s)
	if err == nil {
		t.Error("err == nil")
	}
}
